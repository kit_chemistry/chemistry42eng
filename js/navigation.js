var timeout = [],
	pauseTimeout = 0;

var fadeLauncherIn, fadeLauncherOut,
	fadeNavsIn, fadeNavsOut, fadeTimerOut, startButtonListener,
	toggleInstruction, fadeGreySkinOut, fadeNavsInAutoNGS, fadeNavsInAuto,
	theFrame, theClone, timerTimeout;

var startNavTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var result = "";
		
		result += secondsLeft;
		
		jqueryElement.html(result);
		
		timerTimeout = setTimeout(function(){
			startNavTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}
	
$(document).ready(function(){
	var navs = $(".nav"),
		smallNavs = $(".small-nav"),
		greySkin = $(".nav-grey-skin"),
		launcher = $(".nav-launcher"),
		repeat = $(".nav-repeat"),
		timer = $(".nav-timer"),
		pause = $(".nav-pause");
	
	greySkin.fadeOut(0);
	navs.fadeOut(0);
	
	fadeGreySkinOut = function(){
		greySkin.fadeOut(0);
	}
	
	fadeTimerOut = function(){
		timer.fadeOut(0);
	}
	
	fadeNavsOut = function(){		
		smallNavs.fadeOut(50);
		greySkin.fadeOut(50);
	}
	fadeNavsIn = function(){
		greySkin.fadeIn(50);
		navs.fadeIn(50);
		timer.fadeOut(0);
		pause.fadeOut(0);
		launcher.fadeOut(0);
	}
	
	fadeNavsInAuto = function(){
		greySkin.fadeIn(500);
		timer.fadeIn(500);
		smallNavs.addClass("nav-center");
		navs.fadeIn(500);
		pause.fadeIn(500);
		launcher.fadeOut(0);
		timeout[0] = setTimeout(function(){
			smallNavs.addClass("transition-0-5s");
			smallNavs.removeClass("nav-center");
		}, 500);
		timeout[1] = setTimeout(function(){
			smallNavs.removeClass("transition-0-5s");
		}, 2000)
				
		startNavTimer(timer, 5, function(){
			hideEverythingBut($("#" + theFrame.attr("data-next")));
		}, 3000);
	}
	
	fadeNavsInAutoNGS = function(){
		timer.fadeIn(500);
		smallNavs.addClass("nav-center");
		navs.fadeIn(500);
		pause.fadeIn(500);
		launcher.fadeOut(0);
		timeout[0] = setTimeout(function(){
			smallNavs.addClass("transition-0-5s");
			smallNavs.removeClass("nav-center");
		}, 500);
		timeout[1] = setTimeout(function(){
			smallNavs.removeClass("transition-0-5s");
		}, 2000)
				
		startNavTimer(timer, 5, function(){
			hideEverythingBut($("#" + theFrame.attr("data-next")));
		}, 3000);
	}
	
	fadeLauncherIn = function(){
		launcher.fadeIn(0);
	}
	fadeLauncherOut = function(){
		launcher.fadeOut(0);
	}
	
	toggleInstruction = function(){
		greySkin.toggleClass("nav-grey-skin-instruction");
	}
	
	var navListener = function(){
		var currElem = $(this);
		
		clearTimeout(timerTimeout);
		
		if(currElem.hasClass("nav-home"))
		{
			hideEverythingBut($("#frame-000"));
		}
		else if(currElem.hasClass("nav-ff"))
		{
			hideEverythingBut($("#" + theFrame.attr("data-next")));
		}
		else if(currElem.hasClass("nav-fb"))
		{
			hideEverythingBut($("#" + theFrame.attr("data-prev")));
		}
		else if(currElem.hasClass("nav-close"))
		{
			fadeNavsOut();
			fadeTimerOut();
			launcher.fadeIn(200);
		}
		else if(currElem.hasClass("nav-repeat"))
		{
			hideEverythingBut(theFrame);
			fadeNavsOut(0);
			timeout[0] = setTimeout(function(){
				fadeLauncherIn();
			}, 1000);
		}
		else if(currElem.hasClass("nav-pause"))
		{
			clearTimeout(timerTimeout);
			pause.fadeOut(0);
			timer.fadeOut(0);
		}
		else if(currElem.hasClass("nav-launcher"))
		{
			fadeNavsIn();
		}
		else if(currElem.hasClass("nav-instruction"))
		{
			toggleInstruction();
		}
	}
	navs.off("click", navListener);
	navs.on("click", navListener);	
});

var textFontResizer = function(){
	var mwidth = parseInt($("#main").css("width"));
	
	$("body").css("font-size", mwidth / 20 + "px");
}

